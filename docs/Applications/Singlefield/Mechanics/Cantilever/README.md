# Cantilever

Here, we will analyse a cantilever with pressure load, and will perform all four analysis types

* Static analysis (click [here](cantileverStatic.xml) for the xml file)
* Transient analysis (click [here](cantileverTransient.xml) for the xml file)
* Harmonic analysis (click [here](cantileverHarmonic.xml) for the xml file)
* Eigenfrequency analysis (click [here](cantileverEigenfrequency.xml) for the xml file)
    
Here you can download the [mesh](cantilever.cfs) and [material](mat.xml) file.

---

## Setup
We consider a cantilever, which is fixed (fully supported) on the left and is loaded on the top surface by a pressure load.
![a1](Setup.jpg)



---

## Meshing using Ansys
As a preprocessor we use ANSYS Classic and setup our geometric model and meshing with the PREP7 commands (here in upper case letters) and openCFS macro (here in lower case letters).
For more information, use the following ANSYS macro command reference table

command | description
------------ | ------------
*init* | Initialized the mesh-extension
*setelems,type,[order]* | Select element type for meshing. Valid types are '2d-line', '3d-line', 'triangle', 'quadr', 'tetra', 'brick', 'pyramid' and 'wedge'. Valid orders are 'linear' (=default) and 'quad'
*welems,name* | Write selected elements with name attribute *name*
*wnodes* |  Write selected nodes
*wnodbc,name* |  Write selected nodal boundary conditions with name attribute *name*
*wsavnod,name* | Write selected nodes for saving nodal results with attribute *name*
*wsavelem,name* | Write selected elements for saving element results with attribute *name*
*mkmesh* | Write *.mesh file





* Initialization part
```fortran
 FINI
 /UIS,msgpop,3         ! disable warning popups
 /CLEAR
 /FILNAME,cantilever,1 ! important: define name of the model
 /PREP7
 /UIS,msgpop,2         ! enable warning popups
 init                  ! initialize macros for mesh-interface to CFS++.
                       ! note: all macro commands here are written in lower case letters
```

* Define constants and generate geometry
```fortran
 ! define constants
 eps=1e-7
 L=400e-6
 b=2e-6
 bc=45e-6
 ! generate the cantilever
 BLOCK,0,L,0,bc,0,b
```

* Perform volume meshing
```fortran
 setelems,'brick'
 ESIZE,5e-6
 VSEL,s,,,1
 VMESH,all
```

* Perform surface mesh for pressure load and mechanical fixing
```fortran
 setelems,'quadr'
 ALLSEL
 ASEL,s,,,2
 AMESH,all
 ALLSEL
 ASEL,s,loc,x,-eps,eps
 ASEL,r,loc,y,-eps,bc+eps
 ASEL,r,loc,z,-eps,eps+b
 AMESH,all
```

* Display of mesh
![a2](mesh.jpg)


* Write volume elements
```fortran
 ALLSEL,all
 VSEL,s,,,1
 ESLV
 welems,'cantilever'
```

* Write surface elements for clamping and pressure load
```fortran
 ! write surface elements for clampuing
 ALLSEL
 ASEL,s,,,2
 ESLA
 welems,'excite' 
 ! write surface elements for mechanical fixing 
 ALLSEL 
 ASEL,s,loc,x,-eps,eps 
 ASEL,r,loc,y,-eps,bc+eps 
 ASEL,r,loc,z,-eps,eps+b+b 
 ESLA 
 welems,'fix-all'
```

* Write coordinates of nodes
```fortran
 ALLSEL 
 wnodes
```

* Write the mesh as *.mesh file
```fortran
 mkmesh
```

Within this directory you should now find the file cantilever.mesh, which can now be converted by executing the following
```
cfstool -m convert cantilever.mesh cantilever.h5
```

You can have a look at the contents of the HDF5 mesh file using [HDFView](https://www.hdfgroup.org/solutions/hdf5/)
![a2](ExampleMech3.jpg)


---

## Static Analysis
Setup the XML file for describing the physics and simulation parameter.

Use an XML editor (setup is described [here](../../../../Installation/XML-Editors.md) and the usage is described [here](../../../../Tutorials/XMLExplanations/editing.md) 

The structure of this xml file is as follows:
* Header
```
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
```

* File section, defining input and output files. For a list of maintained input mesh formats click [here](https://opencfs.gitlab.io/userdocu/Tutorials/XMLExplanations/structure/#fileformats-section). The file extension .h5 (or .hdf5) can be used interchangeable with .cfs extension, it basically is exactly the same, just a different extension name.
```
  <fileFormats> 
    <input> 
      <hdf5 fileName="cantilever.h5"/> 
    </input> 
    <output> 
      <hdf5/> 
      <text id="txt"/>
    </output> 
    <materialData file="mat.xml"/>
  </fileFormats>
```

* Define computational domain with volume-regions, surface-regions, ..; with the `nodeList`, we select a node within the mesh, at which we can later on save, e.g., the displacement in a history file (ASCII format).
```
  <domain geometryType="3d">
      <region material="Steel" name="cantilever"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="excite"/>
      <surfRegion name="fix-all"/>
    </surfRegionList>
    <nodeList>
      <nodes name="record">
        <coord x="400e-6" y="0" z="0"/>
      </nodes>
    </nodeList>
  </domain>
```

* Start the sequence and define the analysis type 
```xml
 <sequenceStep>
    <analysis>
      <static/>
    </analysis>

```

* Define the partial differential equation (PDE) and the region it is defined in
```xml
 <pdeList>
      <mechanic subType="3d"> 
        <regionList>
          <region name="cantilever"/>
        </regionList>
```

* Define the boundary conditions
```xml
        <bcsAndLoads>
          <pressure name="excite" value="100" /> 
          <fix name="fix-all">
            <comp dof="x"/>
            <comp dof="y"/>
            <comp dof="z"/>
          </fix>
        </bcsAndLoads>
```

* Define which results to be stored. You can also select to write the result only on specific regions
```xml
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
           <nodeList>
             <nodes name="record"/>
           </nodeList>
          </nodeResult> 
          <elemResult type="mechStress">
            <allRegions/>
          </elemResult>
        </storeResults>
```

* Close PDE part
```xml
      </mechanic>
   </pdeList>
```

* Close the sequence step and the global xml definition
```xml
  </sequenceStep>
 </cfsSimulation>
```


* Make sure the material definition in the material-xml file `mat.xml` is defined correctly
```xml 
<?xml version='1.0' encoding='utf-8'?>
<cfsMaterialDataBase xmlns="http://www.cfs++.org/material">
  <material name="Steel">
    <mechanical>
      <density>
        <linear>
          <real> 7.8E03 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real> 210E09 </real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.3 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
  </material>
</cfsMaterialDataBase>
```

Now, we are ready and can perform the computation by runing `cfs cantileverStatic`. This will result in the following screen output:
![a4](ExampleMech4.jpg)


Furthermore, the following output files have been generated:
* In `results_hdf5` folder: cantileverStatic.h5: contains the simulation result
* In `history` folder: cantileverStatic-ms1-mechDisplacement-node-91-record.hist
* In current directory: cantileverStatic.info.xml


Now, we can start `paraview` and display the deformed structure.
![a5](ExampleMech5.jpg)


---

## Transient Analysis
Perform a transient analysis, we just have to change within our XML-description file the following sections
* Change analysis part
```xml
    <analysis>
      <transient>
        <numSteps> 40 </numSteps>
        <deltaT> 0.05 </deltaT>
      </transient>
    </analysis>
```

* Change in bcsAndLoads for pressure excitation (sine-excitation with a frequency of 100Hz)
```xml
 <bcsAndLoads>
   <pressure name="excite“ value="100*sin(2*pi*1*t)" />
```


---

## Harmonic Analysis
For the harmonic analysis, we perform the follwoing changes
* Change analysis part
```xml
     <analysis>
      <harmonic>
        <numFreq> 10 </numFreq>
        <startFreq> 10 </startFreq>
        <stopFreq> 1e4 </stopFreq>
        <sampling>linear</sampling>
      </harmonic>
    </analysis>
```

* Change pressure load towards amplitude and phase
```xml
        <bcsAndLoads>
          <pressure name="excite“ value="100" phase="0.0" />
```


---



## Eigenfrequency Analysis
For the eigenfreuqency analysis, we perform the follwoing changes
* Change analysis part
```xml
    <analysis>
      <eigenFrequency>
        <isQuadratic> no </isQuadratic>
        <numModes> 5 </numModes>
        <freqShift> 0 </freqShift>
        <writeModes> yes </writeModes>
      </eigenFrequency>
    </analysis>
```
No load is needed (not necessarly be changed).
We are ready and can perform the computation by runing `cfs cantileverEigenfrequency`.
This will result in the following screen output:
![a6](ExampleMech6.jpg)

Start `paraview` and visualize the different mode shapes
![a7](ExampleMech7.jpg)

# Perfectly Matched Layer (PML)

Similar to an Absorbing Boundary Condition (ABC) the PML is used for the treatment of open domain problems. Taking a look at the acoustic PDE for example, the soundhard as well as the soundsoft boundary condition completely reflect impinging waves, hence, special boundary conditions need to be used to deal with problems where the sound should be radiated freely outwards of the computational domain. 

## Theoretical background based on the acoustic PDE

If an orthogonlly impinging wave is expected (e.g. in a channel), an ABC will be sufficient and more efficient regarding the simulation time, since no additional elements are required. If one treats a more general problem, where not only orthognally impinging waves are expected, a PML will be the better solution (although additional elements are required which will inherently cause a larger system matrix as well as longer computational time). The main idea of a PML is a complex ansatz for the speed of sound as well as the density inside the PML region given by [@Kaltenbacher2010].
\begin{equation}
\underline{c} = \frac{c}{1-j \sigma_0}
\end{equation}
and
\begin{equation}
\underline{\rho} = \rho (1-j \sigma_0).
\end{equation}
The acoustic impdeance is given by $Z=\underline{\rho} \underline{c} = \rho c$ and is perfectly matched to the acoustic region. Due to the complex valued speed of sound, the wave number is also complex valued, which leads to a damping part in the solution of the system. Since the matching is only ideal for an orthogonally impinging wave, a decomposition with individual damping in x-, y- and z-direction can be done. For the damping function multiple options can be chosen (constant, quadratic, and inverse distance), although it can be proofen, that the inverse distance function offers the optimal damping function for the Helmholtz equation. A highly efficient and stable implementation is provided for transient simulations, see [@BMKALTENBACHER2013]. 

## Usage of the PML
 
In order to assign the PML properly one has to assign its ID to the region in the ```<regionList>```. The PML used in the ```<dampingList>``` has to have a matching ID regarding the one used for the PML-region and can be defined in openCFS as follows:
```
<region name="PML" dampingId="myPML" polyId="pID" integId="intID"/>
```

```
<dampingList>
	<pml id="myPml">
		<type>inverseDist</type>
		<dampFactor>1.0</dampFactor>
	</pml>
</dampingList>
```

As always in the XML-scheme one has to pay attention to where the ```<dampingList>``` tag has to be defined. If no additional features like Non-Conforming Interfaces are used, the ```<dampingList>``` tag can be used directly after defining the regions via the ```<regionList>``` tag. When using higher order elements or even a mixture of orders, the ID of the polynomial as well as the integration order can be set by defining ```polyId``` as well as ```integID``` respectively. One has to pay attention that the integration order is suitable for the polynomail order.

If multiple PMLs are used, so called "bounding boxes" have to be defined. These bounding boxes are used to identify the volume regions used for the computation of the damping factors. The bounding box can be defined individually for each PML inside the ```<pml>``` tag as follows:
```
<pml id="myPml">
	<propRegion>
		<direction comp="x" min="x_min" max="x_max"/>
		<direction comp="y" min="y_min" max="y_max"/>
		<direction comp="z" min="z_min" max="z_max"/>
	</propRegion>
	<type>inverseDist</type>
	<dampFactor>1.0</dampFactor>
</pml>
```

The values assigned for the minimum and maximum x-, y- and z-coordinate are the ones of the adjoining propagation region.

## Important things to consider when using a PML

Regarding the usage of a PML the following points have to be considered:

* The spatial discretization in the PML-region should be the same as in the propagation region. 
* About four 1$^{\mathrm{st}}$ order finite elements or two quadratic ones along the thickness of the PML-layer are enough to achieve good results.
* The thickness of the PML should be constant.
* The orientation of the PML with respect to the impinging wave should be as favourable as possible (normal to the wave), since the damping properties of the PML get worse for a more tangential impinging wave.

## Examples

Here you can find links to exemplary applications which use the concept of a PML:

[Sound-Barrier](../../Applications/Singlefield/Acoustics/SoundBarrier/README.md)

# References
\bibliography

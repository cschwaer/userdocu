## Mechanic-acoustic coupling

In the case of mechanic-acoustic coupling we have two PDEs, each defined on their respective domain(s). On the one hand, we have the mechanics-PDE where we solve for the balance of momentums, and on the other hand the acoustics-PDE where we solve the acoustic wave equation. The mechanic domain is coupled with the acoustic domain at a common interface, hence we are dealing with a so called surface or interface coupling. In general, we distinguish between two types of coupling:

* **Strong coupling:** In this case, the mechanical field and the acoustic field including their couplings have to be solved simultaneously, since the retroactive effect of both fields on each other is not negligible. This is for example the case when we are dealing with very high sound pressure levels (e.g. piezoelectric ultrasound arrays in water).
* **Weak coupling:**
In the case where the pressure forces of the fluid on the solid are negligible, a sequential computation is sufficient. In a first step, the mechanical problem is solved, which is then used in a second step for the acoustic PDE as a source term based on the movement of the body.

### Coupling conditions
In order to obtain a formulation for the direct mechanic-acoustic coupling we take the mechanic PDE as well as the acoutic PDE and extend them by coupling terms. These coupling terms result from the interface conditions between the two domains and can be split into a kinematic and a dynamic one.

* **Kinematic coupling condition:** At a solid-fluid interface continuity requires that the normal component of the mechanical surface velocity of the solid has to conincide with the normal component of the acoustic particle velocity of the fluid. This condition reads as
\begin{equation}
\mathbf{n} \cdot (\mathbf{v}-\mathbf{v}_{\mathrm{a}})=0
\end{equation}
and can be rewritten in terms of the solution variable of the mechanical PDE as well as  solution of the acoustic PDE in the pressure-formulation
\begin{equation}
\mathbf{n} \cdot \bigg(\frac{\partial^{2} \mathbf{u}}{\partial t^{2}} + \frac{1}{\rho_0} \nabla p_a \bigg)  = 0
\end{equation}
as well as
\begin{equation}
\mathbf{n} \cdot \left( \frac{\partial \mathbf{u}}{\partial t} + \nabla \psi_a \right) = 0
\end{equation}
for the acoustic PDE in the potential-formulation.

* **Dynamic coupling conditions:** Additionally to the continuity of the normal velocity we also have to ensure the continuity of forces accross the shared interface. The continuity of forces reads as
\begin{equation}
\mathbf{\sigma} \cdot \mathbf{n} = -p_\mathrm{a} \mathbf{n}
\end{equation}
and can also be rewritten as before for the acoustic PDE in the potential-formulation as
\begin{equation}
\mathbf{\sigma} \cdot \mathbf{n} = - \rho_0 \frac{\partial \psi_\mathrm{a}}{\partial t}.
\end{equation}

### Interface definition
Since each domain has it's own definition regarding the direction of the normal vector (pointing out of the domain), we have to use a unified normal vector definition for the common interface. This is usually done by introducing the common normal vector 
\begin{equation}
\mathbf{n} = \mathbf{n}_s = - \mathbf{n}_a
\end{equation}
With this shared normal vector, which can also be seen in the following figure, we can 

![acoustic](mech_acou_normal.png){: style="height:25mm;width:40mm" : .center}

*Normal vector definition at the shared interface between the mechanical domain and the acoustic domain*

### Weak formulation
For simplicity we assume homogeneous Dirichlet- and Neumann boundary conditions for both PDEs. Furhtermore, we assume that the only source term for the acoustic PDE is given by the fluid-solid interaction. The correct function space for the weak form of the mechanic PDE in $\mathbb{R}^d$ as well as for the acoustic PDE (both pressure- and potential-formulation) is given by

\begin{equation}
\label{eq:defVspace}
V:=\lbrace
\mathbf{u} \in [H^1 (\Omega)]^d: \textrm{tr}_{\Gamma_\mathrm{D}} (\mathbf u) = 0\, \textrm{on}\, \Gamma_\mathrm{D}
\rbrace.
\end{equation}

Multiplying the the mechanic PDE with the test function $\mathbf{u}^\prime \in V$ as well as the acoustic PDE in the pressure formulation with $p^\prime \in V$, integrating over their respective domains and incorporating the interface conditions as well as the boundary conditions finally leads to the coupled system: Find $\mathbf{u} \in V$ and $p_\mathrm{a} \in V$ such that

\begin{equation}
\label{eq:mechAcouPotential}
\begin{aligned}
	\int_{\Omega_\mathrm{m}} \mathbf{u}^\prime \cdot \rho \frac{\partial^2 \mathbf{u}}{\partial t^2} \mathrm{d\Omega} + \frac{1}{2} \int_{\Omega_\mathrm{m}} \nabla \mathbf{u}^\prime : [\mathbf{C}] : \left( \nabla \mathbf{u} + (\nabla \mathbf{u})^T \right) \mathrm{d\Omega}\\ + \int_{\Gamma_\mathrm{ma}} \mathbf{u}^\prime \cdot \mathbf{n} \ p_\mathrm{a} \ \mathrm{d\Gamma} &= \int_{\Omega_\mathrm{m}} \mathbf{u}^\prime \cdot \mathbf{f}_\mathrm{V} \ \mathrm{d\Omega} \quad &\forall \mathbf{u}^\prime \in V,\\
	\int_{\Omega_\mathrm{a}} p^\prime \frac{1}{c_0^2} \frac{\partial^2 p_\mathrm{a}}{\partial t^2} \mathrm{d\Omega} + \int_{\Omega_\mathrm{a}} \nabla p^\prime \cdot \nabla p_\mathrm{a} \ \mathrm{d\Omega} - \int_{\Gamma_\mathrm{ma}} p^\prime \ \rho_0 \frac{\partial^2 \mathbf{u}}{\partial t^2} \cdot \mathbf{n} \ \mathrm{d\Gamma} &= 0 \quad &\forall p^\prime \in V.
\end{aligned}
\end{equation}

For the acoustic PDE in the potential-formulation we can perform the same steps by multiplying with the test function $\psi^\prime \in V$: Find $\mathbf{u} \in V$ and $\psi_\mathrm{a} \in V$ such that

\begin{equation}
\label{eq:mechAcouPressure}
\begin{aligned}
	\int_{\Omega_\mathrm{m}} \mathbf{u}^\prime \cdot \rho \frac{\partial^2 \mathbf{u}}{\partial t^2} \mathrm{d\Omega} + \frac{1}{2} \int_{\Omega_\mathrm{m}} \nabla \mathbf{u}^\prime : [\mathbf{C}] : \left( \nabla \mathbf{u} + (\nabla \mathbf{u})^T \right) \mathrm{d\Omega}\\ + \int_{\Gamma_\mathrm{ma}} \mathbf{u}^\prime \cdot \mathbf{n} \ \rho_0 \frac{\partial \psi_\mathrm{a}}{\partial t} \mathrm{d\Gamma} &= \int_{\Omega_\mathrm{m}} \mathbf{u}^\prime \cdot \mathbf{f}_\mathrm{V} \ \mathrm{d\Omega} \quad &\forall \mathbf{u}^\prime \in V,\\
	\int_{\Omega_\mathrm{a}} \psi^\prime \frac{1}{c_0^2} \frac{\partial^2 \psi_{\mathrm{a}}}{\partial t^2} \mathrm{d\Omega} + \int_{\Omega_\mathrm{a}} \nabla \psi^\prime \cdot \nabla \psi_{\mathrm{a}} \ \mathrm{d\Omega} - \int_{\Gamma_\mathrm{ma}} \psi^\prime \frac{\partial \mathbf{u}}{\partial t} \cdot \mathbf{n} \ \mathrm{d\Gamma} &= 0 \quad &\forall \psi^\prime \in V.
\end{aligned}
\end{equation}

for the potential formulation.

### Analysis Types

Depending on the temporal setting, the pressure- as well as the potential-formulation can be used for the following cases:

* *Harmonic-*case ($\partial / \partial t (\cdot) = j \omega (\cdot)$)
```
<analysis>
	<harmonic>
		<numFreq>10</numFreq>
		<startFreq>10</startFreq>
		<stopFreq>3000</stopFreq>
		<sampling>linear</sampling>
	</harmonic>
</analysis>
```
* *Transient-*case ($\partial / \partial t (\cdot) \neq 0$) 
```
<analysis>
  <transient>
		<numSteps>100</numSteps>
		<deltaT>1e-3</deltaT>
  </transient>
</analysis>
```

### PDE types

Depending on the desired formulation one can choose between the pressure- and the potential-formulation regarding the acoustic PDE. For the coupled problem the mechanic PDE and the acoustic PDE (regardless of the formulation) have to be defined in the ```<pdeList>``` as follows:

* Pressure formulation
```
<pdeList>
	<mechanic subType="3d">
		...
	</mechanic>
	...
	<acoustic formulation="acouPressure">
		..
	</acoustic>
	...
</pdeList>
```

* Potential formulation
```
<pdeList>
	<mechanic subType="3d">
		...
	</mechanic>
	...
	<acoustic formulation="acouPotential">
		..
	</acoustic>
	...
</pdeList>
```

### Defining the coupling
* **Strong coupling:** In order to define a strong coupling between the mechanic PDE and the acoustic PDE, the ```<couplingList>``` can be used. We define a direct (strong) coupling between the PDEs at one or multiple shared interfaces as:
```
<couplingList>
	<direct>
		<acouMechDirect>
			<surfRegionList>
				<surfRegion name="IF_MECH_ACOU"/>
				...
			</surfRegionList>
		</acouMechDirect>
	</direct>
</couplingList>
```

* **Weak coupling:** For the weak coupling we can define two seperate sequence steps, where we solve the mechanical problem in the first step and use the result as a source term for the second one, where we solve the acoustic problem.

### Material and postprocessing results

For the definition of the individual material, the boundary conditions as well as the postprocessing results please refer to the individual description of the PDE covered in the *Singlefield*-section. It has to be noted, that the individual materials for the two PDEs can be combined in a single material file.
```
<material name="Air">
	<acoustic>
		...
	</acoustic>
	...
</material>

<material name="Si8">
	<mechanical>
		...
	</mechanical>
	...
</material>
```


# Cell2Node and Node2Cell

## Node2Cell
The node to cell interpolation filter takes nodal loads and connects them to the cell center, of the cell defined by those nodes
\begin{equation}
e_{\square} = \sum_{i=1}^\mathrm{n}  v_i \, .
\end{equation}
Thereby, $e_\square$ is the load located to the cell, $\mathrm{n}$ the number of nodes of one element, and $v_i$ the nodal loads. 
The following example shows this methodology by considering one tetrahedral element:
![Cell2Node](cell2node.png)
```
    <interpolation type="FieldInterpolation_Cell2Node" id="..." inputFilterIds="...">
      <targetMesh>
        <hdf5 fileName="..."/>
      </targetMesh>
      <singleResult>
        <inputQuantity resultName="..."/>
        <outputQuantity resultName="..."/>
      </singleResult>
      <regions>
        <sourceRegions>
          <region name="..." />
        </sourceRegions>
        <targetRegions>
          <region name="..."/>
        </targetRegions>
      </regions>
    </interpolation>

```
Note: 
* It is important that the target mesh is ** not ** the same mesh as the source mesh. Transform source mesh into new, empty mesh by using cfs -g. 
* It is possible to interpolate from a volume onto (curved) surfaces. However, the input data can not be from curved surfaces!

## Cell2Node
The cell to node interpolation filter takes element loads and divides it onto the nodes that build the cell.  
\begin{equation}
v_{\square} = \frac{1}{\mathrm{n}} e_i \, .
\end{equation}
The following example shows this methodology by considering one tetrahedral element:
![Node2Cell](node2cell.png)
```
    <interpolation type="FieldInterpolation_Node2Cell" id="..." inputFilterIds="...">
      <targetMesh>
        <hdf5 fileName="..."/>
      </targetMesh>
      <singleResult>
        <inputQuantity resultName="..."/>
        <outputQuantity resultName="..."/>
      </singleResult>
      <regions>
        <sourceRegions>
          <region name="..." />
        </sourceRegions>
        <targetRegions>
          <region name="..."/>
        </targetRegions>
      </regions>
    </interpolation>

```
Note: 
* It is important that the target mesh is ** not ** the same mesh as the source mesh. Transform source mesh into new, empty mesh by using cfs -g. 
* It is possible to interpolate from a volume onto (curved) surfaces. However, the input data can not be from curved surfaces!

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [FE-based Interpolation]._



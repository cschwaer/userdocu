# Time Mean

The Time Mean filter computes the forward moving average $r(t)$ of a time series $s(t)$ with a defined span $n=$ **meanSpan** timesteps.

$r(t) = \frac{1}{n} \sum^{n-1}_{i=0} x(t-i\cdot t_s)$

with the sampling time $t_s$, which is defined in the input filter (see [Data Input/Output](../../Tutorials/Features/mathexpressions.md))

```
<timeMean id="mean" inputFilterIds="input" meanSpan="1">
  <singleResult>
	<inputQuantity resultName="..."/>
	<outputQuantity resultName="..."/>
  </singleResult>
</timeMean>
```

---

## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [Time Mean]._

---
# openCFS userdocu

This is the user documentation for [openCFS](https://opencfs.org), an open source finite element software focusing on multiphysics simulation (reflected by its name, an acronym for **open** source **C**oupled **F**ied **S**imulation).
This research-driven simulation tool features elaborate techniques for various physical fields. The modelling strategy focuses on physical fields and their respective couplings. Strong data exchange interfaces are provided to use third-party pre- and post-processing libraries. The XML-based modelling language is easy to handle and well described by application examples.


This documentation aims to provide a starters-guide to openCFS.
It includes:

* Installation guides (Section [Installation](./Installation/))
* Introduction into the various physical fields (Section [PDE Explanations](./PDEExplanations/Singlefield/AcousticPDE/))
* Tutorials on how our xml scheme works or how the analysis workflow is structured (Section [Tutorials](./Tutorials/AnalysisWorkflow/))
* Fully functional application examples, providing a step-by-step solution of multiphysical simulation examples (Section [Applications](./Applications/Singlefield/Acoustics/SoundBarrier/))

---

The documentation currently only covers a small sub-set of the functionality of openCFS.
**We're happy about contributions to the documentation**, for details head over to the [source code respository of the userdocu](https://gitlab.com/openCFS/userdocu).

---
<img src="spiralspule_result.jpg" width="250">
<img src="Drossel.png" width="300">
<img src="magVentil.png" width="300">
<img src="Transformer.png" width="300">
<img src="powerElectronics.jpg" width="300">
<img src="piezoStack.png" width="275">
<img src="wAVES.png" width="250">
<img src="Lautsprecher.png" width="350">
<img src="SoundRadiation.png" width="250">
<img src="apeAirfoilField.png" width="250">
<img src="CAA_Vent1.png" width="300">
<img src="AxialFan_SourceTerms_BPF.png" width="300">
<img src="humanVoiceSource1.png" width="500">






